# odoo-nginx-pg

## Instale odoo, con PostgreSQL como SGBD, mas nginx como proxy inverso y certbot para SSL

Procure usar en lo posible las últimas versiones de docker y docker-compose.

Una vez descargado este repositorio, seguir estos pasos:

### Reemplazar temporalmente el contenido de nginx/config/default.conf
```
server {
    listen [::]:80;
    listen 80;

    location ~ /.well-known/acme-challenge {
        allow all;
        root /var/www/html;
    }

    location / {
        proxy_set_header X-Real-IP  $remote_addr;
        proxy_set_header X-Forwarded-For $remote_addr;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header Host $host;
        proxy_pass http://odoo:8069;
    }

    location ~* /web/static/ {
        proxy_set_header X-Real-IP  $remote_addr;
        proxy_set_header X-Forwarded-For $remote_addr;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header Host $host;
        proxy_pass http://odoo:8069;
    }
}
```
### Modificar los archivos .env y config/odoo.conf con las contraseñas que correspondan

### Modificar el archivo docker-compose.yml con los datos que correspondan en el container certbot

### Levantar el docker-compose.yml

 docker-compose up -d

###  Esperar a que certbot valide el sitio. Verificar con 
 
 docker logs certbot

### Volver a modificar el archivo  nginx/config/default.conf (no olvidar reemplazar el dominio)
```
server {
    listen [::]:80;
    listen 80;
    add_header Strict-Transport-Security max-age=2592000;
    rewrite ^/.*$ https://$host$request_uri? permanent;

}

server {
    listen [::]:443 ssl http2;
    listen 443 ssl http2;

    server_name tudominio.com;

    ssl_certificate /etc/nginx/ssl/live/tudominio.com/fullchain.pem;
    ssl_certificate_key /etc/nginx/ssl/live/tudominio.com/privkey.pem;

    location ~ /.well-known/acme-challenge {
        allow all;
        root /var/www/html;
    }

    location / {
        proxy_set_header X-Real-IP  $remote_addr;
        proxy_set_header X-Forwarded-For $remote_addr;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header Host $host;
        proxy_pass http://odoo:8069;
    }

    location ~* /web/static/ {
        proxy_set_header X-Real-IP  $remote_addr;
        proxy_set_header X-Forwarded-For $remote_addr;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header Host $host;
        proxy_pass http://odoo:8069;
    }
}
```
### Reiniciar el container nginx

 docker restart nginx

 

